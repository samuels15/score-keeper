# -*- coding: utf-8 -*-
import pandas as pd
import sys

df = pd.read_csv("votacao.csv", sep=";");
print (df.head())

try:
    f = open(sys.argv[1], 'r')
    pessoas = f.readlines()
except:
    print ("Arquivo invalido.");
    sys.exit(1)

for pessoa in pessoas:
    pessoa = pessoa.replace("\n",'')
    if len(df.loc[(df.votante==pessoa)])==0:
        df = df.append({'votante':pessoa, 'pontos':0}, ignore_index=True);
    df.loc[(df.votante == pessoa),'pontos']+=1

print (df.head())
df.to_csv('votacao.csv', sep= ';', index=False)
